package utils

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func ContainsInt(a []int, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
